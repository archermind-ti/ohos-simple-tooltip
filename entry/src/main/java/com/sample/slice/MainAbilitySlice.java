package com.sample.slice;

import io.github.douglasjunior.ohossimpletooltip.OverlayView;
import io.github.douglasjunior.ohossimpletooltip.PopupWindow;
import io.github.douglasjunior.ohossimpletooltip.SimpleTooltip;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import com.sample.ResourceTable;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"MainAbilitySlice");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_fab).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_animated).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_simple).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_overlay).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_outside).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_overlay_rect).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_maxwidth).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_modal_custom).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_inside).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_custom_arrow).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_no_arrow).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_dialog).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_inside_modal).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_btn_center).setClickedListener(this::onClick);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component v) {
        if (v.getId() == ResourceTable.Id_fab) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text("Floating Action Button")
                    .gravity(LayoutAlignment.START)
                    .onDismissListener(new SimpleTooltip.OnDismissListener() {
                        @Override
                        public void onDismiss(SimpleTooltip tooltip) {
                            HiLog.info(logLabel,"dismiss" + tooltip);
                        }
                    })
                    .onShowListener(new SimpleTooltip.OnShowListener() {
                        @Override
                        public void onShow(SimpleTooltip tooltip) {
                            HiLog.info(logLabel,"show" + tooltip);
                        }
                    })
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_simple) {

            HiLog.info(logLabel,"Id_btn_simple onClick");
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_simple)
                    .gravity(LayoutAlignment.END)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_animated) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_animated)
                    .gravity(LayoutAlignment.TOP)
                    .animated(true)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_overlay) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_overlay)
                    .gravity(LayoutAlignment.START)
                    .animated(true)
                    .transparentOverlay(false)
                    .overlayWindowBackgroundColor(Color.BLACK.getValue())
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_maxwidth) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(getString(ResourceTable.String_btn_maxwidth) + getString(ResourceTable.String_btn_maxwidth) + getString(ResourceTable.String_btn_maxwidth) + getString(ResourceTable.String_btn_maxwidth) + getString(ResourceTable.String_btn_maxwidth))
                    .gravity(LayoutAlignment.END)
                    .maxWidth(ResourceTable.String_simpletooltip_max_width)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_outside) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_outside)
                    .gravity(LayoutAlignment.BOTTOM)
                    .dismissOnOutsideTouch(true)
                    .dismissOnInsideTouch(false)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_inside) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_inside)
                    .gravity(LayoutAlignment.START)
                    .dismissOnOutsideTouch(false)
                    .dismissOnInsideTouch(true)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_inside_modal) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_inside_modal)
                    .gravity(LayoutAlignment.END)
                    .dismissOnOutsideTouch(false)
                    .dismissOnInsideTouch(true)
                    .modal(true)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_modal_custom) {
            final SimpleTooltip tooltip = new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_modal_custom)
                    .gravity(LayoutAlignment.TOP)
                    .dismissOnOutsideTouch(false)
                    .dismissOnInsideTouch(false)
                    .modal(true)
                    .animated(true)
                    .animationDuration(2000)
                    .animationPadding(ResourceTable.String_animator_padding)
                    .contentView(ResourceTable.Layout_tooltip_custom, ResourceTable.Id_tv_text)
                    .focusable(true)
                    .build();

            final TextField ed = tooltip.findViewById(ResourceTable.Id_ed_text);
            tooltip.findViewById(ResourceTable.Id_btn_next).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (tooltip.isShowing())
                        tooltip.dismiss();
                    new SimpleTooltip.Builder(v.getContext())
                            .anchorView(v)
                            .text(ed.getText())
                            .gravity(LayoutAlignment.BOTTOM)
                            .build()
                            .show();
                }
            });

            tooltip.show();
        } else if (v.getId() == ResourceTable.Id_btn_no_arrow) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_no_arrow)
                    .gravity(LayoutAlignment.START)
                    .showArrow(false)
                    .modal(true)
                    .animated(true)
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_custom_arrow) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_custom_arrow)
                    .gravity(LayoutAlignment.END)
                    .modal(true)
                    .arrowDrawable(ResourceTable.Media_ic_media_previous)
                    .arrowHeight(AttrHelper.vp2px(45,getContext()))
                    .arrowWidth(AttrHelper.vp2px(30,getContext()))
                    .build()
                    .show();

        } else if (v.getId() == ResourceTable.Id_btn_dialog) {
            final PopupWindow dialog = new PopupWindow(getContext(),null);
            Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_dialog,null,false);
            dialog.setCustomComponent(component);
            int offsetX = AttrHelper.vp2px(getContext().getResourceManager().getDeviceCapability().width,getContext());
            int offsetY = AttrHelper.vp2px(getContext().getResourceManager().getDeviceCapability().height,getContext());
            HiLog.info(logLabel,"screen width %{public}d,height %{public}d",offsetX/2-component.getEstimatedWidth()/2,offsetY/2);
            dialog.setAutoClosable(true);
            dialog.showOnCertainPosition(LayoutAlignment.TOP|LayoutAlignment.LEFT,260,offsetY/2);

            final Button btnInDialog = (Button) component.findComponentById(ResourceTable.Id_btn_in_dialog);
            btnInDialog.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    new SimpleTooltip.Builder(getContext())
                            .anchorView(btnInDialog)
                            .text(ResourceTable.String_btn_in_dialog)
                            .gravity(LayoutAlignment.BOTTOM)
                            .animated(true)
                            .transparentOverlay(false)
                            .overlayMatchParent(false)
                            .mAnchorViewContainer(dialog)
                            .build()
                            .show();
                }
            });
            final Button btnClose = (Button) component.findComponentById(ResourceTable.Id_btn_close);
            btnClose.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    dialog.destroy();
                }
            });
        } else if (v.getId() == ResourceTable.Id_btn_center) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_center)
                    .showArrow(false)
                    .gravity(LayoutAlignment.CENTER)
                    .build()
                    .show();
        } else if (v.getId() == ResourceTable.Id_btn_overlay_rect) {
            new SimpleTooltip.Builder(this)
                    .anchorView(v)
                    .text(ResourceTable.String_btn_overlay_rect)
                    .gravity(LayoutAlignment.END)
                    .animated(true)
                    .transparentOverlay(false)
                    .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                    .overlayOffset(0)
                    .build()
                    .show();
        }
    }
}
