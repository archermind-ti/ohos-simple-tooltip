package io.github.douglasjunior.ohossimpletooltip;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class ArrowDrawable extends ShapeElement {
    HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0x00201,"ArrowDrawable");

    public static final int LEFT = 0,TOP = 1,RIGHT = 2,BOTTOM  =3 ,AUTO = 4;
    private Paint paint = new Paint();
    private final int backgroundColor;
    private Path path;
    private final int direction;
    private Rect bounds;
    private float mArrowWidth;
    private float mArrowHeight;

    public ArrowDrawable(int foregroundColor, int direction,float arrowWidth, float arrowHeight) {
        this.backgroundColor = Color.TRANSPARENT.getValue();
        paint.setColor(new Color(foregroundColor));
        this.direction = direction;
        this.mArrowWidth = arrowWidth;
        this.mArrowHeight = arrowHeight;

        setCallback(new OnChangeListener() {
            @Override
            public void onChange(Element element) {
                Rect bds = element.getBounds();
                if (bounds != null) {
                    if(bounds.bottom != bds.bottom || bounds.left != bds.left || bounds.right != bds.right || bds.top != bounds.top){
                        bounds = bds;
                        updatePath(bounds);
                    }
                }else {
                    bounds = bds;
                    updatePath(bounds);
                }
            }
        });
    }

    private  void updatePath(Rect bounds) {
        path = new Path();
        float width = this.mArrowWidth;
        float height = this.mArrowHeight;
        switch (direction){
            case LEFT:{
                path.moveTo(height,width);
                path.lineTo(0,width / 2);
                path.lineTo(height,0);
                path.lineTo(height,width);
                break;
            }
            case TOP:
                path.moveTo(0,height);
                path.lineTo(width/ 2, 0);
                path.lineTo(width, height);
                path.lineTo(0, height);
                break;
            case RIGHT:
                path.moveTo(0, 0);
                path.lineTo(height, width / 2);
                path.lineTo(0, width);
                path.lineTo(0, 0);
                break;
            case BOTTOM:
                path.moveTo(0, 0);
                path.lineTo(width / 2, height);
                path.lineTo(width, 0);
                path.lineTo(0, 0);
                break;
        }
        path.close();
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        HiLog.info(logLabel,"drawToCanvas");

        canvas.drawColor(backgroundColor,BlendMode.OVERLAY);
        if(path == null){
            updatePath(getBounds());
        }
        canvas.drawPath(path,paint);
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setRgbColor(RgbColor color) {
        paint.setColor(new Color(color.asArgbInt()));
    }

    public void setColorFilter(ColorFilter filter){
        paint.setColorFilter(filter);
    }

    @Override
    public int getAlpha() {
        if(paint.getColorFilter() != null){
            return Color.TRANSPARENT.getValue();
        }
        return paint.getColor().getValue() >>> 24;

    }
}
