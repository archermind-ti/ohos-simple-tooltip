/*
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2016 Douglas Nassif Roma Junior
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package io.github.douglasjunior.ohossimpletooltip;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

import static ohos.agp.components.Component.*;
import static ohos.multimodalinput.event.TouchEvent.PRIMARY_POINT_DOWN;

public class SimpleTooltip implements TouchEventListener {


    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"SimpleTooltip");
    // Default Resources
    private static final int mDefaultBackgroundColorRes = ResourceTable.Color_simpletooltip_background;
    private static final int mDefaultTextColorRes = ResourceTable.Color_simpletooltip_text;
    private static final int mDefaultArrowColorRes = ResourceTable.Color_simpletooltip_arrow;
    private static final int mDefaultMarginRes = ResourceTable.String_simpletooltip_margin;
    private static final int mDefaultPaddingRes = ResourceTable.String_simpletooltip_padding;
    private static final int mDefaultAnimationPaddingRes = ResourceTable.String_simpletooltip_animation_padding;
    private static final int mDefaultAnimationDurationRes = ResourceTable.Integer_simpletooltip_animation_duration;
    private static final int mDefaultArrowWidthRes = ResourceTable.String_simpletooltip_arrow_width;
    private static final int mDefaultArrowHeightRes = ResourceTable.String_simpletooltip_arrow_height;
    private static final int mDefaultOverlayOffsetRes = ResourceTable.String_simpletooltip_overlay_offset;

    private final Context mContext;
    private OnDismissListener mOnDismissListener;
    private OnShowListener mOnShowListener;
    private PopupWindow mPopupWindow;
    private PopupWindow mAnchorViewContainer;
    private final int mGravity;
    private final int mArrowDirection;
    private final boolean mDismissOnInsideTouch;
    private final boolean mDismissOnOutsideTouch;
    private final boolean mModal;
    private final Component mContentView;
    private Component mContentLayout;
    private final int mTextViewId;
    private final int mOverlayWindowBackgroundColor;
    private final String mText;
    private final Component mAnchorView;
    private final boolean mTransparentOverlay;
    private final float mOverlayOffset;
    private final boolean mOverlayMatchParent;
    private final float mMaxWidth;
    private Component mOverlay;
    private ComponentContainer mRootView;
    private final boolean mShowArrow;
    private Image mArrowView;
    private final Element mArrowDrawable;
    private final boolean mAnimated;
    private AnimatorGroup mAnimator;
    private final float mMargin;
    private final float mPadding;
    private final float mAnimationPadding;
    private final long mAnimationDuration;
    private final float mArrowWidth;
    private final float mArrowHeight;
    private final boolean mFocusable;
    private boolean dismissed = false;
    private int mHighlightShape;
    private int width;
    private int height;
    private boolean mIgnoreOverlay;
    private SimpleTooltip simpleTooltip;
    private boolean drawArrowDrawable;


    private SimpleTooltip(Builder builder) {
        mContext = builder.context;
        mGravity = builder.gravity;
        mOverlayWindowBackgroundColor=builder.overlayWindowBackgroundColor;
        mArrowDirection = builder.arrowDirection;
        mDismissOnInsideTouch = builder.dismissOnInsideTouch;
        mDismissOnOutsideTouch = builder.dismissOnOutsideTouch;
        mModal = builder.modal;
        mContentView = builder.contentView;
        mTextViewId = builder.textViewId;
        mText = builder.text;
        mAnchorView = builder.anchorView;
        mTransparentOverlay = builder.transparentOverlay;
        mOverlayOffset = builder.overlayOffset;
        mOverlayMatchParent = builder.overlayMatchParent;
        mMaxWidth = builder.maxWidth;
        mShowArrow = builder.showArrow;
        mArrowWidth = builder.arrowWidth;
        mArrowHeight = builder.arrowHeight;
        mArrowDrawable = builder.arrowDrawable;
        mAnimated = builder.animated;
        mMargin = builder.margin;
        mPadding = builder.padding;
        mAnimationPadding = builder.animationPadding;
        mAnimationDuration = builder.animationDuration;
        mOnDismissListener = builder.onDismissListener;
        mOnShowListener = builder.onShowListener;
        mFocusable = builder.focusable;
        mRootView = SimpleTooltipUtils.findFrameLayout(mAnchorView);
        mHighlightShape = builder.highlightShape;
        mIgnoreOverlay = builder.ignoreOverlay;
        this.width = builder.width;
        this.height = builder.height;
        simpleTooltip = this;
        this.mAnchorViewContainer = builder.mAnchorViewContainer;
        drawArrowDrawable = builder.drawArrowDrawable;
        init();
    }

    private void init() {
        configPopupWindow();
        configContentView();
    }

    private void configPopupWindow() {
        mPopupWindow = new PopupWindow(mContext,null);
        mPopupWindow.setSize(width,height);
        mPopupWindow.setBackColor(Color.TRANSPARENT);
        mPopupWindow.setTransparent(true);
        mPopupWindow.setAutoClosable(mDismissOnOutsideTouch);
        mPopupWindow.setOnCloseListener(dialogCloseListener);
    }


    public void show() {
        verifyDismissed();

        mContentLayout.setLayoutRefreshedListener(mLocationLayoutListener);

        if (mRootView.getVisibility() == VISIBLE) {
            showPopupWindow();
        }
    }

    private void showPopupWindow(){
        int[] location = mAnchorView.getLocationOnScreen();
        int offsetX = location[0];
        int offsetY = location[1];
        if (mAnchorViewContainer != null && mAnchorViewContainer.getAlignment() == (LayoutAlignment.TOP|LayoutAlignment.LEFT)){
            offsetX += mAnchorViewContainer.getOffsetx();
            offsetY += mAnchorViewContainer.getOffsetY();
        }
        mContentLayout.estimateSize(Component.EstimateSpec.getSize(EstimateSpec.PRECISE),Component.EstimateSpec.getSize(EstimateSpec.PRECISE));
        switch (mGravity) {
            case LayoutAlignment.START:
                mPopupWindow.showOnCertainPosition(LayoutAlignment.TOP|LayoutAlignment.LEFT,offsetX-mContentLayout.getEstimatedWidth(), offsetY-(mContentLayout.getEstimatedHeight()-mAnchorView.getEstimatedHeight())/2);
                break;
            case LayoutAlignment.END:
                mPopupWindow.showOnCertainPosition(LayoutAlignment.TOP|LayoutAlignment.LEFT,offsetX+mAnchorView.getEstimatedWidth(), offsetY-(mContentLayout.getEstimatedHeight()-mAnchorView.getEstimatedHeight())/2);
                break;
            case LayoutAlignment.TOP:
                mPopupWindow.showOnCertainPosition(LayoutAlignment.TOP|LayoutAlignment.LEFT,offsetX, offsetY - mContentLayout.getEstimatedHeight());
                break;
            case LayoutAlignment.BOTTOM:
                mPopupWindow.showOnCertainPosition(LayoutAlignment.TOP|LayoutAlignment.LEFT,offsetX, offsetY +mAnchorView.getEstimatedHeight());
                break;
            case LayoutAlignment.CENTER:
                mPopupWindow.showOnCertainPosition(LayoutAlignment.CENTER,0, 0);
                break;
            default:
                throw new IllegalArgumentException("Gravity must have be CENTER, START, END, TOP or BOTTOM.");
        }
    }

    private void verifyDismissed() {
        if (dismissed) {
            throw new IllegalArgumentException("Tooltip has been dismissed.");
        }
    }

    private void createOverlay() {
        if (mIgnoreOverlay) {
            return;
        }
        mOverlay = mTransparentOverlay ? new Component(mContext) : new OverlayView(mContext, mAnchorView, mHighlightShape, mOverlayOffset,mOverlayWindowBackgroundColor);
        if (mOverlayMatchParent)
            mOverlay.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        else
            mOverlay.setLayoutConfig(new ComponentContainer.LayoutConfig(mRootView.getWidth(), mRootView.getHeight()));
        mOverlay.setTouchEventListener(mOverlayTouchListener);
        mRootView.addComponent(mOverlay);
    }

    private void configContentView() {
        if (mContentView instanceof Text) {
            Text tv = (Text) mContentView;
            tv.setText(mText);
        } else {
            Text tv = (Text) mContentView.findComponentById(mTextViewId);
            if (tv != null)
                tv.setText(mText);
        }

        mContentView.setPadding((int) mPadding, (int) mPadding, (int) mPadding, (int) mPadding);

        DirectionalLayout linearLayout = new DirectionalLayout(mContext);
        linearLayout.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));

        linearLayout.setOrientation(mArrowDirection == ArrowDrawable.LEFT || mArrowDirection == ArrowDrawable.RIGHT ? HORIZONTAL : VERTICAL);
        int layoutPadding = (int) (mAnimated ? mAnimationPadding : 0);
        linearLayout.setPadding(layoutPadding, layoutPadding, layoutPadding, layoutPadding);

        if (mShowArrow) {
            mArrowView = new Image(mContext);

            if (drawArrowDrawable){
                PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
                if (mArrowDirection == ArrowDrawable.TOP || mArrowDirection == ArrowDrawable.BOTTOM) {
                    options.size = new Size((int)mArrowWidth, (int)mArrowHeight);
                } else {
                    options.size = new Size((int)mArrowHeight, (int)mArrowWidth);
                }
                options.pixelFormat = PixelFormat.ARGB_8888;
                PixelMap output = PixelMap.create(options);
                Canvas canvas = new Canvas(new Texture(output));
                canvas.save();
                mArrowDrawable.drawToCanvas(canvas);
                canvas.restore();
                mArrowView.setPixelMap(output);
            }else {
                mArrowView.setImageElement(mArrowDrawable);
            }

            DirectionalLayout.LayoutConfig arrowLayoutParams;

            if (mArrowDirection == ArrowDrawable.TOP || mArrowDirection == ArrowDrawable.BOTTOM) {
                arrowLayoutParams = new DirectionalLayout.LayoutConfig((int) mArrowWidth, (int) mArrowHeight,LayoutAlignment.HORIZONTAL_CENTER,0);
            } else {
                arrowLayoutParams = new DirectionalLayout.LayoutConfig((int) mArrowHeight, (int) mArrowWidth,LayoutAlignment.HORIZONTAL_CENTER,0);
            }
            mArrowView.setLayoutConfig(arrowLayoutParams);

            if (mArrowDirection == ArrowDrawable.BOTTOM || mArrowDirection == ArrowDrawable.RIGHT) {
                linearLayout.addComponent(mContentView);
                linearLayout.addComponent(mArrowView);
            } else {
                linearLayout.addComponent(mArrowView);
                linearLayout.addComponent(mContentView);
            }
        } else {
            linearLayout.addComponent(mContentView);
        }

        DirectionalLayout.LayoutConfig contentViewParams = new  DirectionalLayout.LayoutConfig(width, height, LayoutAlignment.CENTER,0);
        mContentView.setLayoutConfig(contentViewParams);
        mContentView.estimateSize(Component.EstimateSpec.getSize(EstimateSpec.PRECISE),Component.EstimateSpec.getSize(EstimateSpec.PRECISE));
        if (mMaxWidth > 0 && mContentView.getEstimatedWidth() > mMaxWidth) {
            SimpleTooltipUtils.setWidth(mContentView, mMaxWidth);
            if (mContentView instanceof Text){
                ((Text)mContentView).setMultipleLine(true);
            }
        }

        mContentLayout = linearLayout;
        mContentLayout.setTouchEventListener(this::onTouchEvent);
        mContentLayout.setVisibility(VISIBLE);
        mPopupWindow.setCustomComponent(mContentLayout);
    }

    public void dismiss() {
        if (dismissed)
            return;

        dismissed = true;
        if (mPopupWindow != null) {
            mPopupWindow.remove();
        }
    }

    /**
     * <div class="pt">Indica se o tooltip está sendo exibido na tela.</div>
     * <div class=en">Indicate whether this tooltip is showing on screen.</div>
     *
     * @return <div class="pt"><tt>true</tt> se o tooltip estiver sendo exibido, <tt>false</tt> caso contrário</div>
     * <div class="en"><tt>true</tt> if the popup is showing, <tt>false</tt> otherwise</div>
     */
    public boolean isShowing() {
        return mPopupWindow != null && mPopupWindow.isShowing();
    }

    public <T extends Component> T findViewById(int id) {
        //noinspection unchecked
        return (T) mContentLayout.findComponentById(id);
    }

    private final Component.TouchEventListener mOverlayTouchListener = new Component.TouchEventListener() {
        @Override
        public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
            HiLog.info(logLabel,"mOverlayTouchListener mModal : "+mModal);
            return mModal;
        }
    };

    private final Component.LayoutRefreshedListener mLocationLayoutListener = new Component.LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            HiLog.info(logLabel,"mLocationLayoutListener mMaxWidth :%{public}f",mMaxWidth);
            final PopupWindow popup = mPopupWindow;
            if (popup == null || dismissed) return;

            if (mOnShowListener != null)
                mOnShowListener.onShow(SimpleTooltip.this);
            mOnShowListener = null;

            popup.getComponent().setLayoutRefreshedListener(null);
            popup.getComponent().setLayoutRefreshedListener(mAnimationLayoutListener);
            createOverlay();
        }
    };

    private final Component.LayoutRefreshedListener mAnimationLayoutListener = new Component.LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            final PopupWindow popup = mPopupWindow;
            if (popup == null || dismissed) return;

            if (mAnimated) startAnimation();

        }
    };

    private void startAnimation() {
        final String property = mGravity == LayoutAlignment.TOP || mGravity == LayoutAlignment.BOTTOM ? "translationY" : "translationX";

        final AnimatorValue anim1 = new AnimatorValue();
        anim1.setDuration(mAnimationDuration);
        anim1.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        anim1.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if ("translationY".equalsIgnoreCase(property)){
                    mContentLayout.setTranslationY(-mAnimationPadding+mAnimationPadding * v * 2);
                }else if ("translationX".equalsIgnoreCase(property)){
                    mContentLayout.setTranslationX(-mAnimationPadding+mAnimationPadding * v * 2);
                }
            }
        });

        final AnimatorValue anim2 = new AnimatorValue();
        anim2.setDuration(mAnimationDuration);
        anim2.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        anim1.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if ("translationY".equalsIgnoreCase(property)){
                    mContentLayout.setTranslationY(mAnimationPadding-mAnimationPadding * v * 2);
                }else if ("translationX".equalsIgnoreCase(property)){
                    mContentLayout.setTranslationX(mAnimationPadding-mAnimationPadding * v * 2);
                }
            }
        });

        mAnimator = new AnimatorGroup();
        mAnimator.runSerially(anim1, anim2);
        mAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (!dismissed && isShowing()) {
                    animator.start();
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mAnimator.start();
    }

    /**
     * <div class="pt">Listener utilizado para chamar o <tt>SimpleTooltip#dismiss()</tt> quando a <tt>View</tt> root é encerrada sem que a tooltip seja fechada.
     * Pode ocorrer quando a tooltip é utilizada dentro de Dialogs.</div>
     */
    private final Component.LayoutRefreshedListener mAutoDismissLayoutListener = new Component.LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            HiLog.info(logLabel,"mAutoDismissLayoutListener");
            final PopupWindow popup = mPopupWindow;
            if (popup == null || dismissed) return;

            if (mRootView.getVisibility() != VISIBLE) dismiss();
        }
    };

    public PopupWindow.DialogCloseListener dialogCloseListener = new PopupWindow.DialogCloseListener() {
        @Override
        public void closeListener() {
            dismissed = true;
            if (mAnimator != null) {
                mAnimator.setLoopedListener(null);
                mAnimator.setStateChangedListener(null);
                mAnimator.end();
                mAnimator.cancel();
                mAnimator = null;
            }
            if (mRootView != null && mOverlay != null) {
                mRootView.removeComponent(mOverlay);
            }
            mRootView = null;
            mOverlay = null;
            if (mOnDismissListener != null)
                mOnDismissListener.onDismiss(simpleTooltip);
            mOnDismissListener = null;
            mPopupWindow.getComponent().setLayoutRefreshedListener(null);
            mPopupWindow = null;
        }
    };

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (component.equals(mContentLayout)  && (touchEvent.getAction() == PRIMARY_POINT_DOWN)){
            if (mDismissOnInsideTouch){
                dismiss();
                return true;
            }
        }
        return false;
    }

    public interface OnDismissListener {
        void onDismiss(SimpleTooltip tooltip);
    }

    public interface OnShowListener {
        void onShow(SimpleTooltip tooltip);
    }

    public static class Builder {
        private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"SimpleTooltip-Builder");
        private final Context context;
        private boolean dismissOnInsideTouch = true;
        private boolean dismissOnOutsideTouch = true;
        private boolean modal = false;
        private Component contentView;
        private int textViewId = 0;
        private String text = "";
        private Component anchorView;
        private PopupWindow mAnchorViewContainer;
        private int arrowDirection = ArrowDrawable.AUTO;
        private int gravity = LayoutAlignment.BOTTOM;
        private boolean transparentOverlay = true;
        private float overlayOffset = -1;
        private boolean overlayMatchParent = true;
        private float maxWidth;
        private boolean showArrow = true;
        private Element arrowDrawable;
        private boolean drawArrowDrawable = false;
        private boolean animated = false;
        private float margin = -1;
        private float padding = -1;
        private float animationPadding = -1;
        private OnDismissListener onDismissListener;
        private OnShowListener onShowListener;
        private long animationDuration;
        private int backgroundColor;
        private int textColor;
        private int arrowColor;
        private float arrowHeight;
        private float arrowWidth;
        private boolean focusable;
        private int highlightShape = OverlayView.HIGHLIGHT_SHAPE_OVAL;
        private int width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        private int height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        private boolean ignoreOverlay = false;
        private int overlayWindowBackgroundColor=0;

        public Builder(Context context) {
            this.context = context;
        }

        public SimpleTooltip build() throws IllegalArgumentException {
            validateArguments();
            if (backgroundColor == 0) {
                backgroundColor = SimpleTooltipUtils.getColor(context.getResourceManager(), mDefaultBackgroundColorRes);
            }

            if (overlayWindowBackgroundColor == 0) {
                overlayWindowBackgroundColor = Color.BLACK.getValue();
            }
            if (textColor == 0) {
                textColor = SimpleTooltipUtils.getColor(context.getResourceManager(), mDefaultTextColorRes);
            }

            if (contentView == null) {
                Text tv = new Text(context);
                tv.setTextSize(AttrHelper.fp2px(18,context));
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setShape(ShapeElement.RECTANGLE);
                shapeElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
                tv.setBackground(shapeElement);
                tv.setTextColor(new Color(textColor));
                contentView = tv;
            }
            if (arrowColor == 0) {
                arrowColor = SimpleTooltipUtils.getColor(context.getResourceManager(), mDefaultArrowColorRes);
            }
            if (margin < 0) {
                margin = getDimension(mDefaultMarginRes);
            }
            if (padding < 0) {
                padding = getDimension(mDefaultPaddingRes);
            }
            if (animationPadding < 0) {
                animationPadding = getDimension(mDefaultAnimationPaddingRes);
            }
            if (animationDuration == 0) {
                animationDuration = getInteger(mDefaultAnimationDurationRes);
            }
            if (showArrow) {

                if (arrowDirection == ArrowDrawable.AUTO)
                    arrowDirection = SimpleTooltipUtils.tooltipGravityToArrowDirection(gravity);
                if (arrowWidth == 0)
                    arrowWidth = getDimension(mDefaultArrowWidthRes);
                if (arrowHeight == 0)
                    arrowHeight = getDimension(mDefaultArrowHeightRes);
                if (arrowDrawable == null){
                    arrowDrawable = new ArrowDrawable(arrowColor, arrowDirection,arrowWidth,arrowHeight);
                    drawArrowDrawable = true;
                }
            }
            if (highlightShape < 0 || highlightShape > OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR) {
                highlightShape = OverlayView.HIGHLIGHT_SHAPE_OVAL;
            }
            if (overlayOffset < 0) {
                overlayOffset = getDimension(mDefaultOverlayOffsetRes);
            }
            return new SimpleTooltip(this);
        }

        private int getInteger(int resourceId){
            int value = 0;
            try {
                value = context.getResourceManager().getElement(resourceId).getInteger();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
            return  value;

        }
        private int getDimension(int resourceId){
            int dimension = 0;
            try {
                String tmp = context.getResourceManager().getElement(resourceId).getString();
                dimension = AttrHelper.convertDimensionToPix(context,tmp,0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
            return  dimension;
        }
        private void validateArguments() throws IllegalArgumentException {
            if (context == null) {
                throw new IllegalArgumentException("Context not specified.");
            }
            if (anchorView == null) {
                throw new IllegalArgumentException("Anchor view not specified.");
            }
        }

        public Builder setWidth(int width){
            this.width = width;
            return this;
        }

        public Builder setHeight(int height){
            this.height = height;
            return this;
        }

        public Builder contentView(Text textView) {
            this.contentView = textView;
            this.textViewId = 0;
            return this;
        }


        public Builder contentView(Component contentView, int textViewId) {
            this.contentView = contentView;
            this.textViewId = textViewId;
            return this;
        }

        public Builder contentView(int contentViewId, int textViewId) {
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            this.contentView = inflater.parse(contentViewId, null, false);
            this.textViewId = textViewId;
            return this;
        }


        public Builder contentView(int contentViewId) {
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            this.contentView = inflater.parse(contentViewId, null, false);
            this.textViewId = 0;
            return this;
        }


        public Builder dismissOnInsideTouch(boolean dismissOnInsideTouch) {
            this.dismissOnInsideTouch = dismissOnInsideTouch;
            return this;
        }


        public Builder dismissOnOutsideTouch(boolean dismissOnOutsideTouch) {
            this.dismissOnOutsideTouch = dismissOnOutsideTouch;
            return this;
        }


        public Builder modal(boolean modal) {
            this.modal = modal;
            return this;
        }


        public Builder text(String text) {
            this.text = text;
            return this;
        }


        public Builder text(int textRes) {
            try {
                this.text = context.getResourceManager().getElement(textRes).getString();
                HiLog.info(logLabel,"text is %{public}s",this.text);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }


        public Builder anchorView(Component anchorView) {
            this.anchorView = anchorView;
            return this;
        }


        public Builder gravity(int gravity) {
            this.gravity = gravity;
            return this;
        }


        public Builder arrowDirection(int arrowDirection) {
            this.arrowDirection = arrowDirection;
            return this;
        }


        public Builder transparentOverlay(boolean transparentOverlay) {
            this.transparentOverlay = transparentOverlay;
            return this;
        }


        public Builder maxWidth(int maxWidthRes) {
            this.maxWidth = getDimension(maxWidthRes);
            return this;
        }


        public Builder maxWidth(float maxWidth) {
            this.maxWidth = maxWidth;
            return this;
        }


        public Builder animated(boolean animated) {
            this.animated = animated;
            return this;
        }


        public Builder animationPadding(float animationPadding) {
            this.animationPadding = animationPadding;
            return this;
        }


        public Builder animationPadding(int animationPaddingRes) {
            this.animationPadding = getDimension(animationPaddingRes);
            return this;
        }

        public Builder animationDuration(long animationDuration) {
            this.animationDuration = animationDuration;
            return this;
        }


        public Builder padding(float padding) {
            this.padding = padding;
            return this;
        }


        public Builder padding(int paddingRes) {
            this.padding = getDimension(paddingRes);
            return this;
        }



        public Builder margin(float margin) {
            this.margin = margin;
            return this;
        }

        public Builder mAnchorViewContainer(PopupWindow popupWindow){
            this.mAnchorViewContainer = popupWindow;
            return this;
        }

        public Builder margin(int marginRes) {
            this.margin = getDimension(marginRes);
            return this;
        }

        public Builder textColor(int textColor) {
            this.textColor = textColor;
            return this;
        }

        public Builder backgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder overlayWindowBackgroundColor(int overlayWindowBackgroundColor) {
            this.overlayWindowBackgroundColor = overlayWindowBackgroundColor;
            return this;
        }

        public Builder showArrow(boolean showArrow) {
            this.showArrow = showArrow;
            return this;
        }

        public Builder arrowDrawable(Element arrowDrawable) {
            this.arrowDrawable = arrowDrawable;
            return this;
        }

        public Builder arrowDrawable(int drawableRes) {
            Image image = new Image(context);
            image.setPixelMap(drawableRes);
            this.arrowDrawable = new PixelMapElement(image.getPixelMap());
            return this;
        }

        public Builder arrowColor(int arrowColor) {
            this.arrowColor = arrowColor;
            return this;
        }


        public Builder arrowHeight(float arrowHeight) {
            this.arrowHeight = arrowHeight;
            return this;
        }

        public Builder arrowWidth(float arrowWidth) {
            this.arrowWidth = arrowWidth;
            return this;
        }

        public Builder onDismissListener(OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
            return this;
        }

        public Builder onShowListener(OnShowListener onShowListener) {
            this.onShowListener = onShowListener;
            return this;
        }

        /**
         * <div class="pt">Habilita o foco no conteúdo da tooltip. Padrão é <tt>false</tt>.</div>
         * <div class="en">Enables focus in the tooltip content. Default is <tt>false</tt>.</div>
         *
         * @param focusable <div class="pt">Pode receber o foco.</div>
         *                  <div class="en">Can receive focus.</div>
         * @return this
         */
        public Builder focusable(boolean focusable) {
            this.focusable = focusable;
            return this;
        }

        /**
         * <div class="pt">Configura o formato do Shape em destaque. <br/>
         * <tt>{@link OverlayView#HIGHLIGHT_SHAPE_OVAL}</tt> - Destaque oval (padrão). <br/>
         * <tt>{@link OverlayView#HIGHLIGHT_SHAPE_RECTANGULAR}</tt> - Destaque retangular. <br/>
         * </div>
         * <p>
         * <div class="en">Configure the the Shape type. <br/>
         * <tt>{@link OverlayView#HIGHLIGHT_SHAPE_OVAL}</tt> - Shape oval (default). <br/>
         * <tt>{@link OverlayView#HIGHLIGHT_SHAPE_RECTANGULAR}</tt> - Shape rectangular. <br/>
         * </div>
         *
         * @param highlightShape <div class="pt">Formato do Shape.</div>
         *                       <div class="en">Shape type.</div>
         * @return this
         * @see OverlayView#HIGHLIGHT_SHAPE_OVAL
         * @see OverlayView#HIGHLIGHT_SHAPE_RECTANGULAR
         * @see Builder#transparentOverlay(boolean)
         */
        public Builder highlightShape(int highlightShape) {
            this.highlightShape = highlightShape;
            return this;
        }

        /**
         * <div class="pt">Tamanho da margem entre {@link Builder#anchorView(Component)} e a borda do Shape de destaque.
         * Este valor sobrescreve <tt>{}</tt></div>
         * <div class="en">Margin between {@link Builder#anchorView(Component)} and highlight Shape border.
         * This value override <tt>{}</tt></div>
         *
         * @param overlayOffset <div class="pt">Tamanho em pixels.</div>
         *                      <div class="en">Size in pixels.</div>
         * @return this
         * @see Builder#anchorView(Component)
         * @see Builder#transparentOverlay(boolean)
         */
        public Builder overlayOffset(float overlayOffset) {
            this.overlayOffset = overlayOffset;
            return this;
        }

        /**
         * <div class="pt">Define o comportamento do overlay. Utilizado para casos onde a view de Overlay não pode ser MATCH_PARENT.
         * Como em uma Dialog ou DialogFragment.</div>
         * <div class="en">Sets the behavior of the overlay view. Used for cases where the Overlay view can not be MATCH_PARENT.
         * Like in a Dialog or DialogFragment.</div>
         *
         * @param overlayMatchParent <div class="pt">True se o overlay deve ser MATCH_PARENT. False se ele deve obter o mesmo tamanho do pai.</div>
         *                           <div class="en">True if the overlay should be MATCH_PARENT. False if it should get the same size as the parent.</div>
         * @return this
         */
        public Builder overlayMatchParent(boolean overlayMatchParent) {
            this.overlayMatchParent = overlayMatchParent;
            return this;
        }

        /**
         * As some dialogs have a problem when displaying tooltip (like expand/subtract) container, this ignores overlay adding altogether.
         * @param ignoreOverlay flag to ignore overlay adding
         * @return this
         */
        public Builder ignoreOverlay(boolean ignoreOverlay) {
            this.ignoreOverlay = ignoreOverlay;
            return this;
        }

    }
}
