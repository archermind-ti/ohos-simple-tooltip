# ohos_simple_tooltip

#### 项目介绍

- 基于PopupDialog的创建tooltip的一个简单库。

#### 安装教程
在模块下的build.gradle种添加以下信息：

```
​```
dependencies {
	...
	implementation("com.gitee.archermind-ti:ohos-simple-tooltip:1.0.0-beta")
	...
}
​```
```

### 功能演示

<img src="screenshot/simpleTooltip.gif" width=280/>

#### 使用说明

### Java code
            Button btn = findComponentById(ResourceTable.Id_btn_no_arrow).setClickedListener(this::onClick);
            new SimpleTooltip.Builder(this)
                    .anchorView(btn)
                    .text(ResourceTable.String_btn_no_arrow)
                    .gravity(LayoutAlignment.START)
                    .showArrow(false)
                    .modal(true)
                    .animated(true)
                    .build()
                    .show();

#### 原库差异说明
- 为方便定位PopupDialog内的组件位置,SimpleTooltip.Builder增加mAnchorViewContainer(PopupWindow popupWindow)接口

#### 版本迭代


- v1.0.0


#### 版权和许可信息
- The MIT License (MIT)
  
  Copyright (c) 2016 Douglas Nassif Roma Junior

