package io.github.douglasjunior.ohossimpletooltip;

import ohos.agp.components.Component;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class PopupWindow extends PopupDialog{
    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"PopupWindow");

    private Component mComponent;
    private DialogCloseListener mCloseListener;
    private int mAlignment;
    private int mOffsetx;
    private int mOffsetY;

    public PopupWindow(Context context, Component contentComponent) {
        super(context, contentComponent);
        this.mComponent = contentComponent;
    }

    @Override
    public PopupDialog setCustomComponent(Component customComponent) {
        this.mComponent = customComponent;
        return super.setCustomComponent(customComponent);
    }

    public Component getComponent(){
        return this.mComponent;
    }

    public void setOnCloseListener(DialogCloseListener listener){
        this.mCloseListener = listener;
    }

    @Override
    protected void onDestroy() {
        HiLog.info(logLabel,"mPopupWindow onDestroy");
        super.onDestroy();
        if (this.mCloseListener != null){
            mCloseListener.closeListener();
        }
    }

    public interface DialogCloseListener{
        void closeListener();
    }

    @Override
    public void showOnCertainPosition(int alignment, int x, int y) {
        this.mAlignment = alignment;
        this.mOffsetx = x;
        this.mOffsetY = y;
        super.showOnCertainPosition(alignment, x, y);
    }

    public int getOffsetx(){
        return this.mOffsetx;
    }
    public int getOffsetY(){
        return this.mOffsetY;
    }
    public int getAlignment(){
        return this.mAlignment;
    }
}
