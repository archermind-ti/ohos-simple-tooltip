package io.github.douglasjunior.ohossimpletooltip;

import ohos.agp.components.Component;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

public class OverlayView extends Component implements Component.DrawTask {
    private HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP,0X00201,"OverlayView");

    public static final int HIGHLIGHT_SHAPE_OVAL = 0;
    public static final int HIGHLIGHT_SHAPE_RECTANGULAR = 1;

    private Component mAnchorView;
    private PixelMap pixelMap;

    private boolean invalidated = true;
    private final int highlightShape;
    private final float mOffset;
    private final int overlayWindowBackground;

    public OverlayView(Context context, Component anchorComponent, int highlightShape, float offset, int overlayWindowBackground) {
        super(context);
        this.mAnchorView = anchorComponent;
        this.highlightShape = highlightShape;
        this.mOffset = offset;
        this.overlayWindowBackground = overlayWindowBackground;

        addDrawTask(this::onDraw);

    }

    private void createWindowFrame() {
        final int width =getEstimatedWidth(), height = getHeight();
        if (width <= 0 || height <= 0)
            return;
        if (pixelMap != null && !pixelMap.isReleased())
            pixelMap.release();
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width,height);
        options.pixelFormat = PixelFormat.ARGB_8888;
        pixelMap = PixelMap.create(options);

        Canvas osCanvas = new Canvas(new Texture(pixelMap));

        RectFloat outerRectangle= new RectFloat(0,0,width,height);

        Paint paint = new Paint();
        paint.setColor(new Color(overlayWindowBackground));
        paint.setAntiAlias(true);
        paint.setAlpha(0.3f);
        osCanvas.drawRect(outerRectangle,paint);

        paint.setColor(Color.TRANSPARENT);
        paint.setBlendMode(BlendMode.SRC_OUT);

        RectFloat anchorRecr = SimpleTooltipUtils.calculeRectInWindow(mAnchorView);
        RectFloat overlayRecr = SimpleTooltipUtils.calculeRectInWindow(this);

        float left = anchorRecr.left - overlayRecr.left;
        float top = anchorRecr.top - overlayRecr.top;

        RectFloat rect = new RectFloat(left - mOffset, top - mOffset, left + mAnchorView.getEstimatedWidth() + mOffset, top + mAnchorView.getEstimatedHeight() + mOffset);

        if (highlightShape == HIGHLIGHT_SHAPE_RECTANGULAR) {
            osCanvas.drawRect(rect, paint);
        } else {
            osCanvas.drawOval(rect, paint);
        }

        invalidated = false;
    }

    public Component getAnchorView() {
        return mAnchorView;
    }

    public void setAnchorView(Component anchorView) {
        this.mAnchorView = anchorView;
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        HiLog.info(logLabel,"OverlayView-----onDraw---");
        if(invalidated || pixelMap == null || pixelMap.isReleased())
            createWindowFrame();
        if(pixelMap != null && !pixelMap.isReleased())
            canvas.drawPixelMapHolder(new PixelMapHolder(pixelMap),0,0,new Paint());
    }
}
