package com.example;

import com.sample.ResourceTable;
import io.github.douglasjunior.ohossimpletooltip.ArrowDrawable;
import io.github.douglasjunior.ohossimpletooltip.SimpleTooltipUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.LayoutAlignment;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OhosSimpleTooltipTest {
    @Test
    public void testGetColor() {
        int color = SimpleTooltipUtils.getColor(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext().getResourceManager(), ResourceTable.Color_simpletooltip_background);
        Assert.assertNotEquals(color,0);
    }

    @Test
    public void testTooltipGravityToArrowDirection(){
        int direction = SimpleTooltipUtils.tooltipGravityToArrowDirection(LayoutAlignment.START);
        Assert.assertEquals(direction, ArrowDrawable.RIGHT);
    }

}