package io.github.douglasjunior.ohossimpletooltip;

import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public abstract class SimpleTooltipUtils {
    private SimpleTooltipUtils() {
    }

    public static RectFloat calculeRectOnScreen(Component view) {
        int[] location = view.getLocationOnScreen();
        return new RectFloat(location[0], location[1], location[0] + view.getEstimatedWidth(), location[1] + view.getEstimatedHeight());
    }

    public static RectFloat calculeRectInWindow(Component view){
        int[] location = view.getLocationOnScreen();
        return new RectFloat(location[0],location[1],location[0] + view.getEstimatedWidth(), location[1]+ view.getEstimatedHeight());
    }

    public static void setWidth(Component view, float width) {
        ComponentContainer.LayoutConfig config = view.getLayoutConfig();
        if (config == null) {
            config = new ComponentContainer.LayoutConfig((int) width, view.getHeight());
        } else {
            config.width = (int) width;
        }
        view.setLayoutConfig(config);
    }

    public static int tooltipGravityToArrowDirection(int tooltipGravity) {
        switch (tooltipGravity) {
            case LayoutAlignment.START: {
                return ArrowDrawable.RIGHT;
            }
            case LayoutAlignment.END: {
                return ArrowDrawable.LEFT;
            }
            case LayoutAlignment.TOP:{
                return ArrowDrawable.BOTTOM;
            }
            case LayoutAlignment.CENTER:
            case LayoutAlignment.BOTTOM: {
                return ArrowDrawable.TOP;
            }
            default:
                throw new IllegalArgumentException("Gravity must have be CENTER, START, END, TOP or BOTTOM.");
        }
    }

    public static void setX(Component view, int x) {
        ComponentContainer.LayoutConfig config = view.getLayoutConfig();
        config.setMarginLeft(x - config.getMarginLeft());
        view.setLayoutConfig(config);
    }

    public static void setY(Component view, int y) {
        ComponentContainer.LayoutConfig config = view.getLayoutConfig();
        config.setMarginTop(y - config.getMarginTop());
        view.setLayoutConfig(config);
    }

    public static void removeOnGlobalLayoutListener(Component view, ComponentTreeObserver.WindowBoundListener listener){
        view.getComponentTreeObserver().removeWindowBoundListener(listener);
    }

    public static void setTextAppearance(TextField tv, int textAppearanceRes){
//        tv.setComponentDescription();
    }


    private static ComponentContainer.LayoutConfig getOrCreateMarginLayoutConfig(Component view){
        ComponentContainer.LayoutConfig config = view.getLayoutConfig();
        if(config != null){
            return config;
        }else {
            return new ComponentContainer.LayoutConfig(view.getWidth(),view.getHeight());
        }
    }

    public static int getColor(ResourceManager manager, int resourceId){
        int color = 0;
        try {
            color = manager.getElement(resourceId).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return color;
    }
    /**
     * Verify if the first child of the rootView is a FrameLayout.
     * Used for cases where the Tooltip is created inside a Dialog or DialogFragment.
     *
     * @param anchorView
     * @return FrameLayout or anchorView.getRootView()
     */
    public static ComponentContainer findFrameLayout(Component anchorView) {
        ComponentParent rootView = (ComponentParent) anchorView.getComponentParent();
        ComponentContainer rootView1 = null;
        while (rootView != null && rootView.getComponentParent() != null){
            rootView = rootView.getComponentParent();
        }
        if (((ComponentContainer)rootView).getChildCount() == 1 && ((ComponentContainer)rootView).getComponentAt(0) instanceof StackLayout) {
            rootView1 = (ComponentContainer) ((ComponentContainer)rootView).getComponentAt(0);
        }
        return rootView1 == null?((ComponentContainer)rootView):rootView1;
    }

    public static Element getDrawable(Context context, int drawableRes) {
        Element element = null;
        try {
            element = context.getResourceManager().getElement(drawableRes);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return element;
    }

}
